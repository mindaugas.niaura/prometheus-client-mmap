## v0.17.0

- Fix crash when trying to inspect all strings !74

## v0.16.2

- No code changes. Retagging due to extraneous file included in package.

## v0.16.1

- Improve LabelSetValidator debug messages !69
- Properly rescue Oj exceptions !70

## v0.16.0

- Make sure on reset we release file descriptors for open files !63

## v0.15.0

- Make labels order independent !60

## v0.14.0

- Remove deprecated taint mechanism logic !59

## v0.13.0

- Gauge: add decrement method to gauge metric type !57
- Update push.rb to use newest client_ruby code !56

## v0.12.0

- Remove deprecated rb_safe_level() and rb_secure() calls !53

## v0.11.0

- Include filename in IOError exception !47
- Fix clang-format violations !49
- CI: use libasan5 !50
- Truncate MmappedDict#inspect output !51

## v0.10.0

- Eliminate SIGBUS errors in parsing metrics !43
- Make it easier to diagnose clang-format failures !44
- Add Ruby 2.6 and 2.7 to CI builds !45

## v0.9.10

- Extend `Prometheus::Client.reinitialize_on_pid_change` method to receive `:force` param !40
  With `force: true` it reinitializes all metrics files.
  With `force: false` (default) it reinitializes only on changed PID (as it was before).
  In any case, it keeps the registry (as it was before).
  Thus, the change is backward compatible.

## v0.9.9

- Do not allow label values that will corrupt the metrics !38

## v0.9.7

- Restore Prometheus logger !36

## v0.9.7

- Disable warning if prometheus_multiproc_dir is not set !35

## v0.9.6

- Add missing `pid=` label for metrics without labels !31

## v0.9.5

- Set multiprocess_files_dir config to temp directory by default
  https://gitlab.com/gitlab-org/prometheus-client-mmap/merge_requests/28
